package com.pruebamatrices.test;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MultiplicacionMatrices {
	private WebDriver driver;
	
	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://dvalenzuela00.github.io/multiplicacionMatrices/");
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testMatrices() {
		WebElement FilaMatrizA = driver.findElement(By.name("filaA"));
		WebElement ColumnaMatrizA = driver.findElement(By.name("columnaA"));
		WebElement FilaMatrizB = driver.findElement(By.name("filaB"));
		WebElement ColumnaMatrizB = driver.findElement(By.name("columnaB"));
		WebElement crearMatriz = driver.findElement(By.name("crear"));
		FilaMatrizA.clear();
		ColumnaMatrizA.clear();
		FilaMatrizB.clear();
		ColumnaMatrizB.clear();
		
		FilaMatrizA.sendKeys("2");
		ColumnaMatrizA.sendKeys("2");
		FilaMatrizB.sendKeys("2");
		ColumnaMatrizB.sendKeys("2");

		crearMatriz.click();
		
		WebElement MAF1C1 = driver.findElement(By.name("valorA00"));
		WebElement MAF1C2 = driver.findElement(By.name("valorA01"));
		WebElement MAF2C1 = driver.findElement(By.name("valorA10"));
		WebElement MAF2C2 = driver.findElement(By.name("valorA11"));
		
		WebElement MBF1C1 = driver.findElement(By.name("valorB00"));
		WebElement MBF1C2 = driver.findElement(By.name("valorB01"));
		WebElement MBF2C1 = driver.findElement(By.name("valorB10"));
		WebElement MBF2C2 = driver.findElement(By.name("valorB11"));

		WebElement verResultado = driver.findElement(By.name("button"));
		
		MAF1C1.clear();
		MAF1C2.clear();
		MAF2C1.clear();
		MAF2C2.clear();
		
		MBF1C1.clear();
		MBF1C2.clear();
		MBF2C1.clear();
		MBF2C2.clear();
		
		MAF1C1.sendKeys("2");
		MAF1C2.sendKeys("2");
		MAF2C1.sendKeys("2");
		MAF2C2.sendKeys("2");
		
		MBF1C1.sendKeys("2");
		MBF1C2.sendKeys("2");
		MBF2C1.sendKeys("2");
		MBF2C2.sendKeys("2");

		verResultado.click();
		
		
		WebElement RF1C1 = driver.findElement(By.name("valorRes00"));
		WebElement RF1C2 = driver.findElement(By.name("valorRes01"));
		WebElement RF2C1 = driver.findElement(By.name("valorRes10"));
		WebElement RF2C2 = driver.findElement(By.name("valorRes11"));
		
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
		assertEquals("8", RF1C1.getAttribute("value"));
		assertEquals("8", RF1C2.getAttribute("value"));
		assertEquals("8", RF2C1.getAttribute("value"));
		assertEquals("8", RF2C2.getAttribute("value"));
	}
	@After
	public void tearDown() {
		driver.quit();
	}
}
